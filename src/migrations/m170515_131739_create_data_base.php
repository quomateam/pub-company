<?php

use yii\db\Migration;

class m170515_131739_create_data_base extends Migration
{
    public function up()
    {
        $db = quoma\core\helpers\DbHelper::getDbName('db_company');

        $this->execute("CREATE DATABASE `$db`");

    }

    public function down()
    {
        echo "m170515_131739_create_data_base cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
